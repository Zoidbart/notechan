from django.db import models

class Note(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    #TODO:
    # for thread based:
    # id = models.AutoField(primary_key=True) ist per default schon vorhanden, muss nur benutzt werden
    # add childof = id of parent
    # for media attachments
    # add media_body = models.BinaryField()  Hiermit kann man einfach ein Binary blob in die DB pushen.

def __str__(self):
        return '%s %d' (self.id, self.title)

